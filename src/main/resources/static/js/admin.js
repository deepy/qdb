$( document ).ready(function() {

    var token = $("meta[name='_csrf']").attr("content");
    var header = $("meta[name='_csrf_header']").attr("content");

    $(".panel").each(function () {
        var id = $(this).find(".panel-heading a").text().substring(1);
        var adm = $(this).find(".quote-admin");

        var headers = {}
        headers[header] = token;
        adm.find("a[name='approve']").click(function() {
            post({quoteId: id, action: true}, headers, function (data) {
                console.log(data);
            });
            $(this).parents(".panel-default").remove();
        });
        adm.find("a[name='delete']").click(function() {
            post({quoteId: id, action: false}, headers, function (data) {
                console.log(data);
            });
            $(this).parents(".panel-default").remove();
        });
    });

});

function post (data, headers, callback) {
    $.ajax({
        url: '/admin/approve',
        type: 'post',
        data: data,
        headers: headers,
        dataType: 'json',
        success: callback
    });
}