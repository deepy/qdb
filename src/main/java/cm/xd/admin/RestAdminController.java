package cm.xd.admin;

import cm.xd.network.Network;
import cm.xd.network.NetworkRepository;
import cm.xd.quotes.Quote;
import cm.xd.quotes.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by deepy on 13/10/16.
 */
@RestController
@RequestMapping("/admin/api")
public class RestAdminController {
    QuoteRepository quoteRepository;
    NetworkRepository networkRepository;

    @Autowired
    public RestAdminController(QuoteRepository quotes, NetworkRepository nets) {
        quoteRepository = quotes;
        networkRepository = nets;
    }

    @RequestMapping(path = "/quote/pending", method = RequestMethod.GET)
    public List<Quote> pendingQuotes() {
        return quoteRepository.getPending();
    }

    @RequestMapping(path = "/quote", method = RequestMethod.POST)
    public void acceptQuote(@RequestBody int quoteId) {
        quoteRepository.approve(quoteId);
    }

    @RequestMapping(path = "/quote", method = RequestMethod.DELETE)
    public void rejectQuote(@RequestBody int quoteId) {
        quoteRepository.delete(quoteId);
    }

    @RequestMapping(path = "/network", method = RequestMethod.GET)
    public List<Network> getNetworks() {
        return networkRepository.getAllNetworks();
    }

    @RequestMapping(path = "/network", method = RequestMethod.POST)
    public Network addNetwork(@RequestBody Network net) {
        return networkRepository.insert(net);
    }

}
