package cm.xd.admin;

import cm.xd.quotes.QuoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/admin")
@Controller
public class AdminController {
    QuoteRepository quoteRepository;

    @Autowired
    public AdminController(QuoteRepository quoteRepository) {
        this.quoteRepository = quoteRepository;
    }

    @RequestMapping
    public String index(Model model) {
        model.addAttribute("quotes", quoteRepository.getPending());
        return "quote-list";
    }

    @RequestMapping(path = "/approve", method = RequestMethod.GET)
    public String approvalList(Model model) {
        model.addAttribute("quotes", quoteRepository.getPending());
        return "quote-list";
    }

    @RequestMapping(path = "/approve", method = RequestMethod.POST)
    public String approveQuote(int quoteId, Boolean action) {
        if (action != null) {
            if (action) {
                quoteRepository.approve(quoteId);
            }  else {
                quoteRepository.delete(quoteId);
            }
        }
        return "redirect:/admin/approve/";
    }
}
