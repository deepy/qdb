package cm.xd.site;

import cm.xd.network.Network;
import lombok.Data;

/**
 * Created by deepy on 07/08/16.
 */

@Data
public class Domain {
    private final Network network;
    private final String channel;
    private final String name;
}
