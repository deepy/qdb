package cm.xd.site;

import cm.xd.network.Network;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by deepy on 07/08/16.
 */

@Service
public class DomainService {
    protected static final Logger logger = LoggerFactory.getLogger(DomainService.class);


    @Autowired
    JdbcTemplate jdbcTemplate;

    protected Map<String, Domain> domainCache = new HashMap<>();

    public Domain getDomainByName(String name) {
        domainCache.computeIfAbsent(name, k -> getDomain(name));
        return domainCache.get(name);
    }

    protected Domain getDomain(String name) {
        SqlRowSet rowset = jdbcTemplate.queryForRowSet(
                "SELECT s.network, s.channel, n.* FROM sites s" +
                        " LEFT JOIN networks n ON s.network = n.id WHERE s.name = ?",
                name);
        if (rowset.next()) {
            try {
                return new Domain(
                        new Network(
                                rowset.getInt("id"),
                                rowset.getString("name"),
                                null,
                                rowset.getBoolean("public")
                        ), rowset.getString("channel"), name);
            } catch (InvalidResultSetAccessException e) {
                logger.error("Error creating Domain", e);
            }
        }

        return null;
    }
}
