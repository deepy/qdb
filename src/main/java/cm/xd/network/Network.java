package cm.xd.network;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by deepy on 17/07/16.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Network {
    private Integer id;
    private String name;
    private String urlName;
    private boolean publicVisible;
}
