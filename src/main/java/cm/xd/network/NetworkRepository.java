package cm.xd.network;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepy on 17/07/16.
 */

@Repository
public class NetworkRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insert;

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String PUBLIC_VISIBLE = "public";

    @Autowired
    public NetworkRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        insert = new SimpleJdbcInsert(jdbcTemplate).withTableName("networks")
                .usingGeneratedKeyColumns(ID);
    }

    public Network getById(Integer id) {
        return (Network) jdbcTemplate.queryForObject("SELECT *, public as publicvisible FROM NETWORKS WHERE ID = ?",
                new Object[]{id}, new BeanPropertyRowMapper(Network.class));
    }

    public List<Network> getAllNetworks() {
        SqlRowSet rs = jdbcTemplate.queryForRowSet("SELECT * FROM networks");
        return resultExtractor(rs);
    }

    public Network insert(Network network) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        if (network.getName() != null) {
            params.addValue("name", network.getName());
        }
        params.addValue("public", network.isPublicVisible());
        Number id = insert.executeAndReturnKey(params);
        network.setId(id.intValue());
        return network;
    }

    private List<Network> resultExtractor(SqlRowSet rs) {
        List<Network> networks = new ArrayList<>();
        while (rs.next()) {
            Network n = new Network();
            rs.getMetaData().getColumnLabel(1);
            n.setId(rs.getInt(ID));
            n.setName(rs.getString(NAME));
            n.setPublicVisible(rs.getBoolean(PUBLIC_VISIBLE));
            networks.add(n);
        }

        return networks;
    }
}
