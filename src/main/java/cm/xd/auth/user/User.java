package cm.xd.auth.user;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepy on 06/09/16.
 */

@Data
public class User {
    private Integer id;
    private final String email;
    private List<String> roles = new ArrayList<>();

    public User(String email) {
        roles.add("ROLE_USER");
        this.email = email;

    }

    public User(Integer id, String email, boolean admin) {
        this.id = id;
        this.email = email;
        roles.add("ROLE_USER");
        if (admin) {
            roles.add("ROLE_ADMIN");
        }
    }
}
