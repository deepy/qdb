package cm.xd.auth.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepy on 06/09/16.
 */
@Repository
@Lazy
public class UserRepository {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insert;

    PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder encoder) {
        this.passwordEncoder = encoder;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        insert = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("users")
                .usingGeneratedKeyColumns("id");
    }

    public User add(UserDto user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        SqlParameterSource params = new BeanPropertySqlParameterSource(user);
        Number id = insert.executeAndReturnKey(params);
        User account = new User(user.getEmail());
        account.setId(id.intValue());
        return account;
    }

    public org.springframework.security.core.userdetails.User getByUsername(String username) {
        return jdbcTemplate.queryForObject("SELECT * FROM users where email = ?",
                new String[]{username}, new UserMapper());
    }

    public class UserMapper implements RowMapper<org.springframework.security.core.userdetails.User> {

        @Override
        public org.springframework.security.core.userdetails.User mapRow(ResultSet rs, int rowNum)
                throws SQLException {
            String email = rs.getString("email");
            String password = rs.getString("password");
            boolean admin = rs.getBoolean("admin");
            boolean enabled = rs.getBoolean("approved");
            boolean accountNonExpired = true;
            boolean credentialsNonExpired = true;
            boolean accountNonLocked = true;
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            if (admin) {
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            }

            return new org.springframework.security.core.userdetails.User(
                            email, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked,
                            authorities
                    );
        }


    }

}