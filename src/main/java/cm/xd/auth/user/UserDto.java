package cm.xd.auth.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * Created by deepy on 07/08/16.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    @NotEmpty
    private String password;

    private boolean admin = false;
    private boolean approved = false;

    public UserDto(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
