package cm.xd.auth.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deepy on 12/09/16.
 */
@Service
public class UserService implements UserDetailsService {
    private static final String USER_ERROR = "User not found.";

    @Autowired
    UserRepository repo;

    @Override
    public UserDetails loadUserByUsername(String email) {
        User account = repo.getByUsername(email);
        if (account == null) {
            throw new UsernameNotFoundException(USER_ERROR);
        }

        return account;
    }

    @SuppressWarnings("squid:UnusedPrivateMethod")
    private static List<GrantedAuthority> getAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
