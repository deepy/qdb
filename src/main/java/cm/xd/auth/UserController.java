package cm.xd.auth;

import cm.xd.auth.user.UserDto;
import cm.xd.auth.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by deepy on 07/08/16.
 */

@Controller
@RequestMapping("/user")
public class UserController {

    UserRepository repo;

    @Autowired
    public void setUserRepository(UserRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(path = "/register", method = RequestMethod.GET)
    public String registration(WebRequest request, Model model) {
        model.addAttribute("user", new UserDto());
        return "register";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ModelAndView registration(@ModelAttribute("user") @Valid UserDto account,
                                     BindingResult result, WebRequest request, Errors errors) {
        UserDto user = null;
        if (!result.hasErrors()) {
            user = new UserDto("foo", "bar");
        }
        if (user == null) {
            result.rejectValue("email", "Error");
        } else {
            return new ModelAndView("register-success", "user", repo.add(account));
        }
        return new ModelAndView("register", "user", account);
    }
}
