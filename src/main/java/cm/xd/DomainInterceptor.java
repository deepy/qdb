package cm.xd;

import cm.xd.site.Domain;
import cm.xd.site.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by deepy on 19/07/16.
 */

@Component
public class DomainInterceptor extends HandlerInterceptorAdapter {
    public static final String ATTRIBUTE_DEFAULT = "default";
    public static final String ATTRIBUTE_CHANNEL = "channel";
    public static final String ATTRIBUTE_NETWORK = "network";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String DEFAULT_QDB_NAME = "deepy's";

    @Autowired
    private DomainService domainService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String siteName = request.getServerName();
        Domain domain = domainService.getDomainByName(siteName);
        request.setAttribute(ATTRIBUTE_DEFAULT, false);

        if (domain != null) {
            request.setAttribute(ATTRIBUTE_NETWORK, domain.getNetwork().getId());
            request.setAttribute(ATTRIBUTE_NAME, domain.getNetwork().getName());
            String channel = domain.getChannel();
            if (channel != null) {
                request.setAttribute(ATTRIBUTE_CHANNEL, channel);
            }
            return true;
        }
        request.setAttribute(ATTRIBUTE_DEFAULT, true);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) {
        if (modelAndView != null) {
            if (request.getAttribute(ATTRIBUTE_CHANNEL) != null) {
                modelAndView.getModelMap().addAttribute("singleChannel", true);
                modelAndView.getModelMap().addAttribute("title", "#" + request.getAttribute(ATTRIBUTE_CHANNEL));
            } else {
                Object title = request.getAttribute(ATTRIBUTE_NAME);
                modelAndView.getModelMap().addAttribute("title", title != null ? title : DEFAULT_QDB_NAME);
            }
            modelAndView.getModelMap().addAttribute(ATTRIBUTE_DEFAULT, request.getAttribute(ATTRIBUTE_DEFAULT));
        }
    }
}
