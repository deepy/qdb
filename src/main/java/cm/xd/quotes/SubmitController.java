package cm.xd.quotes;

import cm.xd.network.NetworkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by deepy on 28/07/16.
 */
@Controller
@RequestMapping("/submit")
public class SubmitController {

    @Autowired
    NetworkRepository networkRepository;

    @Autowired
    QuoteRepository quoteRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model)
    {
        model.addAttribute("quote", new Quote());
        return "quote-new";
    }

    @RequestMapping(path = "/new", method = RequestMethod.POST)
    public String submit(@ModelAttribute("quote") Quote quote, HttpServletRequest request) {
        Integer network = (Integer) request.getAttribute("network");
        String channel = (String) request.getAttribute("channel");
        if (network != null) {
            quote.setNetwork(networkRepository.getById(network));
        }
        if (channel != null) {
            quote.setChannel(channel);
        }
        quote.setApproved(false);
        quote.setId(quoteRepository.insertById(quote).intValue());
        return "quote-submitted";
    }
}
