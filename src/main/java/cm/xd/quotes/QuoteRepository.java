package cm.xd.quotes;

import cm.xd.network.Network;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by deepy on 17/07/16.
 */

@Repository
public class QuoteRepository {
    public static final String ID = "id";
    public static final String CHANNEL = "channel";
    public static final String CONTENT = "quote";
    private static final String NET_ID = "net_id";

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert insert;

    @Autowired
    public QuoteRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        insert = new SimpleJdbcInsert(jdbcTemplate).withTableName("quotes")
                .usingGeneratedKeyColumns("id");
    }

    public List<Quote> get(Integer network) {
        return query(QuoteQuery.builder().network(network).build());
    }

    public List<Quote> get(Integer network, String channel) {
        return query(QuoteQuery.builder().network(network).channel(channel).build());
    }

    public List<Quote> get(Integer network, String channel, String pathChannel) {
        return query(QuoteQuery.builder()
                .network(network).channel(channel).pathChannel(pathChannel).build());
    }

    public List<Quote> query(QuoteQuery query) {
        SqlRowSet rs;
        boolean defaultSite = false;
        if (query.getId() != null) {
            rs = jdbcTemplate.queryForRowSet("SELECT * FROM quotes q WHERE network = ? AND channel = ? " +
                            "AND net_id = ? AND q.approved = true ORDER BY q.net_id",
                    query.getNetwork(), query.getChannel(), query.getId());
        } else if (query.getChannel() != null) {
            rs = jdbcTemplate.queryForRowSet("SELECT * FROM quotes q WHERE network = ? AND channel = ? " +
                            "AND q.approved = true ORDER BY q.net_id",
                    query.getNetwork(), query.getChannel());
        } else if (query.getNetwork() != null) {
            rs = jdbcTemplate.queryForRowSet("SELECT * FROM quotes q WHERE network = ? AND q.approved = true " +
                            "ORDER BY q.net_id",
                    query.getNetwork());
        } else {
            rs = jdbcTemplate.queryForRowSet("SELECT q.*, n.name, n.id as N_ID FROM quotes q " +
                    "LEFT JOIN networks n ON q.network = n.id " +
                    "WHERE n.public = true " +
                    "AND q.approved = true " +
                    "ORDER BY q.id");
            defaultSite = true;
        }
        return resultExtractor(rs, defaultSite);
    }

    public List<Quote> getPending() {
        List<Quote> ql = new ArrayList<>();
        SqlRowSet rs = jdbcTemplate.queryForRowSet("SELECT " +
                "q.id, q.channel, q.quote, n.id AS N_ID, n.name  " +
                " from quotes q LEFT JOIN networks n ON q.network = n.id" +
                " WHERE q.approved = false");
        while (rs.next()) {
            Quote q = new Quote();
            q.setId(rs.getInt(ID));
            q.setChannel(rs.getString(CHANNEL));
            q.setQuote(rs.getString(CONTENT));
            Network n = new Network();
            n.setId(rs.getInt("n_id"));
            n.setName(rs.getString("name"));
            q.setNetwork(n);
            ql.add(q);
        }
        return ql;
    }

    public List<Quote> getPublic(String id) {
        SqlRowSet rs;
        if (id != null) {
            rs = jdbcTemplate.queryForRowSet("SELECT q.*, n.name, n.id as N_ID, " +
                    "CASE" +
                    "    WHEN s.name IS NULL then s2.name " +
                    "    ELSE s.name " +
                    "END AS url" +
                    " FROM quotes q " +
                    "LEFT JOIN networks n ON q.network = n.id " +
                    "LEFT JOIN sites s ON (n.id = s.network AND s.channel = q.channel) " +
                    "LEFT JOIN sites s2 ON (n.id = s2.network AND s2.channel IS NULL) " +
                    "WHERE n.public = true" +
                    "AND q.id = ? " +
                    "AND q.approved = true", id);
        } else {
            rs = jdbcTemplate.queryForRowSet("SELECT q.*, n.name, n.id as N_ID, " +
                    "CASE" +
                    "    WHEN s.name IS NULL then s2.name " +
                    "    ELSE s.name " +
                    "END AS url" +
                    " FROM quotes q " +
                    "LEFT JOIN networks n ON q.network = n.id " +
                    "LEFT JOIN sites s ON (n.id = s.network AND s.channel = q.channel) " +
                    "LEFT JOIN sites s2 ON (n.id = s2.network AND s2.channel IS NULL) " +
                    "WHERE n.public = true " +
                    "AND q.approved = true " +
                    "ORDER BY q.id");
        }

        return resultExtractor(rs, true);
    }

    public void approve(int id) {
        jdbcTemplate.update("UPDATE quotes SET approved = true WHERE id = ? AND approved = false", id);
    }

    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM quotes WHERE id = ? AND approved = false", id);
    }

    public Quote insert(Quote quote) {
        Number id = insertById(quote);
        quote.setId(id.intValue());
        return quote;
    }

    public Number insertById(Quote quote) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put(CONTENT, quote.getQuote());
        parameters.put(CHANNEL, quote.getChannel());
        parameters.put("network", quote.getNetwork().getId());
        parameters.put("approved", quote.isApproved());
        return insert.executeAndReturnKey(parameters);
    }

    private List<Quote> resultExtractor(SqlRowSet rs, boolean defaultSite) {
        List<Quote> quotes = new ArrayList<>();
        while (rs.next()) {
            Quote q = new Quote();
            rs.getMetaData().getColumnLabel(1);
            q.setId(rs.getInt(ID));
            q.setNetId(rs.getInt(NET_ID));
            q.setChannel(rs.getString(CHANNEL));
            q.setQuote(rs.getString(CONTENT));
            if (defaultSite) {
                Network n = new Network();
                n.setId(rs.getInt("n_id"));
                n.setName(rs.getString("name"));
                n.setUrlName(rs.getString("URL"));
                q.setNetwork(n);
            }
            quotes.add(q);
        }

        return quotes;
    }

    class QuoteMapper implements RowMapper<Quote> {
        @Override
        public Quote mapRow(ResultSet rs, int rowNum) throws SQLException {
            Quote q = new Quote();
            q.setId(rs.getInt(ID));
            q.setNetId(rs.getInt(NET_ID));
            q.setChannel(rs.getString(CHANNEL));
            q.setQuote(rs.getString(CONTENT));
            Network n = new Network();
            n.setId(rs.getInt("n_id"));
            n.setName(rs.getString("name"));
            return q;
        }
    }
}
