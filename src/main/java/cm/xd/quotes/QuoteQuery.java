package cm.xd.quotes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by deepy on 29/09/16.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuoteQuery {
    private Integer id;
    private Integer network;
    private String channel;
    private String pathChannel;

    public void push(String value) {
        if (network == null) {
            network = Integer.valueOf(value);
        } else if (channel == null) {
            channel = value;
        } else if (id == null) {
            id = Integer.valueOf(value);
        }
    }
}
