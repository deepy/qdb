package cm.xd.quotes;

import cm.xd.DomainInterceptor;
import cm.xd.suckitspring.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by deepy on 17/07/16.
 */

@Controller
@RequestMapping("/quotes")
public class QuotesController {
    public static final String MODEL_QUOTES = "quotes";
    public static final String VIEW_QUOTE_LIST = "quote-list";


    private QuoteRepository repo;

    @Autowired
    public QuotesController(QuoteRepository repo) {
        this.repo = repo;
    }

    @RequestMapping("/")
    public String listAll(HttpServletRequest request, Model model) {
        if ((boolean) request.getAttribute(DomainInterceptor.ATTRIBUTE_DEFAULT)) {
            model.addAttribute(MODEL_QUOTES, repo.getPublic(null));
            return VIEW_QUOTE_LIST;
        } else {
            List<Quote> quotes = getQuotes(request);
            model.addAttribute(MODEL_QUOTES, quotes);

            return VIEW_QUOTE_LIST;
        }
    }

    @RequestMapping("/{pathChannel}")
    public String listChannel(HttpServletRequest request,
                                   @PathVariable("pathChannel") String pathChannel,
                                   Model model) {
        return handleRequest(model, request, pathChannel);
    }

    @RequestMapping("/{pathChannel}/{id}")
    public String getQuote(HttpServletRequest request,
                                   @PathVariable("pathChannel") String pathChannel,
                                   @PathVariable("id") String id,
                                   Model model) {
        return handleRequest(model, request, pathChannel, id);
    }

    private String handleRequest(Model model, HttpServletRequest request, String... args) {
        List<Quote> quotes = getQuotes(request, args);
        if (quotes.isEmpty()) {
            throw new NotFoundException();
        }
        model.addAttribute(MODEL_QUOTES, quotes);

        return VIEW_QUOTE_LIST;
    }

    protected List<Quote> getQuotes(HttpServletRequest request, String... args) {
        List<String> arguments = parseRequest(request);
        arguments.addAll(Arrays.asList(args));
        if (arguments.size() > 3) {
            throw new NotFoundException();
        } else {
            QuoteQuery query = new QuoteQuery();
            for (String value : arguments) {
                query.push(value);
            }
            return repo.query(query);
        }
    }

    protected List<String> parseRequest(HttpServletRequest request) {
        List<String> list = new ArrayList<>();
        Integer siteNetwork = (Integer) request.getAttribute(DomainInterceptor.ATTRIBUTE_NETWORK);
        if (siteNetwork != null) {
            list.add(siteNetwork.toString());
            String siteChannel = (String) request.getAttribute(DomainInterceptor.ATTRIBUTE_CHANNEL);
            if (siteChannel != null) {
                list.add(siteChannel);
            }
        }
        return list;
    }
}
