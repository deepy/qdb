package cm.xd.quotes;

import cm.xd.network.Network;
import lombok.Data;

/**
 * Created by deepy on 17/07/16.
 */
@Data
public class Quote {
    private int id;
    private Integer netId;
    private String quote;
    private String channel;
    private Network network;
    private boolean approved;

    public int publicId() {
        if (netId != null) {
            return netId;
        }
        return id;
    }

    @Override
    public String toString() {
        return this.getQuote();
    }
}
