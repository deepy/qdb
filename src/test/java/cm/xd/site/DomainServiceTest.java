package cm.xd.site;

import cm.xd.network.Network;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by deepy on 12/10/16.
 */
public class DomainServiceTest {
    @Test
    public void getCachedDomainByName() throws Exception {
        String domain = "test";
        DomainService ds = new DomainService();
        Domain d = new Domain(new Network(), null, domain);
        ds.domainCache.put(domain, d);
        Assert.assertEquals(d, ds.getDomainByName(domain));
    }

}