package cm.xd.network;

import cm.xd.site.DomainService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by deepy on 27/09/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class NetworkRepositoryTest {
    @Autowired
    NetworkRepository repo;

    @Autowired
    DomainService domain;

    @Test
    public void insert() throws Exception {
        Network net = Network.builder()
                .name("insert")
                .publicVisible(true)
                .build();
        net = repo.insert(net);
        Assert.assertNotNull("Expected ID to be set.", net.getId());
        Assert.assertNotSame(0, net.getId());
    }

    @Test
    public void getById() throws Exception {
        Network net = Network.builder()
                .name("retrieve")
                .publicVisible(true)
                .build();
        net = repo.insert(net);
        Network compare = repo.getById(net.getId());
        Assert.assertEquals("Got unequal network.", net, compare);
    }

}