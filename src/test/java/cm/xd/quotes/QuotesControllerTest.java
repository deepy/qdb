package cm.xd.quotes;

import cm.xd.DomainInterceptor;
import cm.xd.suckitspring.NotFoundException;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by deepy on 03/10/16.
 */
public class QuotesControllerTest {
    QuoteRepository repo = mock(QuoteRepository.class);
    QuotesController controller = new QuotesController(repo);
    ExpectedException expected = ExpectedException.none();

    @Test
    public void allParametersFromWeb() throws Exception {
        controller.getQuotes(mockedRequest(null, null), "1", "channel", "1");
    }

    @Test
    public void noParametersFromWeb() throws Exception {
        controller.getQuotes(mockedRequest(1, "channel"));
    }

    /**
    * Should only accept 1 or 0 parameter from web.
    **/
    @Test
    public void oneParameterFromWeb() throws Exception {
        controller.getQuotes(mockedRequest(1, "channel"), "1");
    }

    /**
     * Default sites have boolean DEFAULT.
     * Network sites have Integer NETWORK
     * Channel sites have String CHANNEL + Integer Network
     */

    @Test(expected = NotFoundException.class)
    public void channelTwoArgs() {
        controller.getQuotes(mockedRequest(1, "channel"), "channel", "1");
    }

    @Test(expected = NotFoundException.class)
    public void tooManyParametersFromWeb() {
        controller.getQuotes(mockedRequest(1, null), "1", "channel", "1");
    }

    public HttpServletRequest mockedRequest(Integer network, String channel) {
        HttpServletRequest request = mock(HttpServletRequest.class);
        if (network != null) {
            when(request.getAttribute(DomainInterceptor.ATTRIBUTE_NETWORK)).thenReturn(network);
            if (channel != null) {
                when(request.getAttribute(DomainInterceptor.ATTRIBUTE_CHANNEL)).thenReturn(channel);
            }
        }

        return request;
    }

}